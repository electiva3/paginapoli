﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AcademyProgress.Models;

namespace AcademyProgress.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult CheckGrades(string numeroDocumento)
        {
            var fakeData = new
            {
                materiasAprobadas = 0.30,
                pendientes = 0.70
            };
            return Json(fakeData);
        }

        [HttpPost]
        public JsonResult CheckExtensionHours(string numeroDocumento)
        {
            var fakeData = new
            {
                horasCompletadas = 0.70,
                pendientes = 0.30
            };
            return Json(fakeData);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
